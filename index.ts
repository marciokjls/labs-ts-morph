import { Project, Scope } from "ts-morph";
import * as fs from "fs";

const project = new Project();
const filePath = './example/example.component.ts';
const sourceFile = project.addSourceFileAtPath(filePath);





sourceFile.getClasses().forEach(classe => {
    console.log(`Classe: ${classe.getName()}`);

    // Itera sobre os métodos
    classe.getMethods().forEach(method => {
        // Se o método não tiver modificador de visibilidade, adiciona "public" ao texto do método
        if (!(method.getModifiers().length > 0)) {
            method.replaceWithText(`public ${method.getText()}`);
        }

        // Imprime informações sobre o método
        console.log(`  Método: ${method.getScope()} ${method.getName()}`);
    });

    console.log(classe.getText());
});


// Sobrescrevendo o arquivo
fs.writeFile(filePath, sourceFile.getFullText(), 'utf8', (err) => {
    if (err) {
      console.error(err);
      return;
    }
    console.log(`Arquivo '${filePath}' foi Atualizado com sucesso.`);
  });


// // Itera sobre as classes no arquivo
// function hasModifiedVisibilityKeyword(method: any): boolean {
//     return method.getModifiers().length > 0;
// }

// // Itera sobre as classes no arquivo
// sourceFile.getClasses().forEach(classe => {
//     console.log(`Classe: ${classe.getName()}`);

//     // Itera sobre os métodos
//     classe.getMethods().forEach(method => {
//         // Verifica se o método tem um modificador de visibilidade
//         const hasVisibility = hasModifiedVisibilityKeyword(method);
        
//         // Imprime informações sobre o método
//         console.log(`  Método: ${method.getName()} - Tem Modificador de Visibilidade: ${hasVisibility}`);
//     });
//});





// const classes = sourceFile.getClasses();

// for (const classe of classes) {
//     console.log(`Classe: ${classe.getName()}`);

//     // Métodos
//     const methods = classe.getMethods();
//     methods.forEach(method => {
//         console.log(`  Método: ${method.getName()} - Scope: ${method.getScope()}`);
//     });

//     // Atributos
//     const properties = classe.getProperties();
//     properties.forEach(property => {
//         console.log(`  Atributo: ${property.getName()} - Scope: ${property.getScope()}`);
//     });
    
//     // Construtores
//     const constructors = classe.getConstructors();
//     constructors.forEach(constructor => {
//         console.log(`  Construtor: ${constructor.getText()}`);
//     });
// }


// function sortMembersByVisibility(members: (MethodDeclaration | any)[]): (MethodDeclaration | any)[] {
//     return members.sort((a, b) => {
//         const visibilityOrder = ['private','public'];

//         const aVisibility = a.getScope();
//         const bVisibility = b.getScope();

//         const aIndex = visibilityOrder.indexOf(aVisibility);
//         const bIndex = visibilityOrder.indexOf(bVisibility);

//         return aIndex - bIndex;
//     });
// }
