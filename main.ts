import { Project, Scope } from "ts-morph";
import * as fs from "fs";

const project = new Project();
const filePath = './example/example.component.ts';
const sourceFile = project.addSourceFileAtPath(filePath);
const classes = sourceFile.getClasses();


for (const classe of classes) {
  console.log(`Classe: ${classe.getName()}`);

  const members: string[] = [];

  // Atributos
  const properties = classe.getProperties();
  properties.forEach(property => {
    if (!(hasModifiedVisibilityKeyword(property))) {
      property.replaceWithText(`public ${property.getText()}`);
    }
  });

  members.push(`\n//${addComentario('Atributos privados')}`)
  const privateProperty = classe.getProperties().filter(m => m.getScope() === Scope.Private);
  privateProperty.forEach(property => {
    members.push(property.getText());
  });


  members.push(`\n//${addComentario('Atributos públicos')}`)
  const publicProperty = classe.getProperties().filter(m => m.getScope() === Scope.Public);
  publicProperty.forEach(property => {
    members.push(property.getText());
  });

  members.push(`\n//${addComentario('Construtor')}`)
  // Construtor
  const constructor = classe.getConstructors();
  constructor.forEach(c => {
    members.push(c.getText());
  });
  
  // Métodos
  const methods = classe.getMethods();
  methods.forEach(method => {
    if (!(hasModifiedVisibilityKeyword(method))) {
      method.replaceWithText(`public ${method.getText()}`);
    }
  });
  
  members.push(`//${addComentario('Metódos privados')}`)
  const privateMethods = classe.getMethods().filter(m => m.getScope() === Scope.Private);
  privateMethods.forEach(method => {
    members.push(method.getText());
  });

  members.push(`//${addComentario('Metódos públicos')}`)
  const publicMethods = classe.getMethods().filter(m => m.getScope() === Scope.Public)
  publicMethods.forEach(method => {
    members.push(method.getText());
  });


  // Limpar membros existentes antes de adicionar os reordenados
  classe.getMembers().forEach(member => {
    member.remove();
  });

  // Adicionar membros reordenados
  members.forEach(member => {
    classe.addMember(member);
  });

  console.log(classe.getText())
}

// Sobrescrevendo o arquivo
fs.writeFile(filePath, sourceFile.getFullText(), 'utf8', (err) => {
  if (err) {
    console.error(err);
    return;
  }
  console.log(`Arquivo '${filePath}' foi Atualizado com sucesso.`);
});

// Verifica o modificaor de acesso
function hasModifiedVisibilityKeyword(member: any): boolean {
    return member.getModifiers().length > 0;
}

// add um comentario
function addComentario(comentario : string): string {
  return comentario
}